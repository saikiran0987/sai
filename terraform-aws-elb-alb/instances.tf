resource "aws_instance" "elb_instance_example1" {
  ami           = lookup(var.ami_id, var.region)
  instance_type = var.instance_type
  subnet_id     = aws_subnet.public_1.id

  # Security group assign to instance
  vpc_security_group_ids = [aws_security_group.elb_sg.id]

  # key name
  key_name = var.key_name

  user_data = <<EOF
		        #!/bin/bash
                sudo apt update -y
                sudo apt install docker.io -y
                sudo service docker start 
                sudo docker run  -p 80:80 nginx:latest	
	EOF

  tags = {
    Name = "EC2-Instance-1"
  }
}


